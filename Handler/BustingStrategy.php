<?php
namespace TamTam\Assets\BustingBundle\Handler;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;

class BustingStrategy implements VersionStrategyInterface
{
    /** @var string Path to the mapper file */
    private $mapperPath;
    /** @var string The bust separator */
    private $separator = '--';
    /** @var array The mapping list */
    private $mapper;

    /**
     * @param string $mapperPath The path to the mapper file.
     */
    public function __construct($mapperPath)
    {
        $this->mapperPath = $mapperPath;
    }

    /**
     * Ensure the directory of mapper cache exists.
     */
    public function checkDir()
    {
        $dir = dirname($this->mapperPath);
        if (!is_dir($dir)) {
            mkdir($dir, 0775, true);
        }
    }

    /**
     * Transform the given url to a busted one if exists in mapper
     *
     * @param string $url The url to bust.
     *
     * @return string The url, busted if a bust exists.
     */
    public function applyVersion($url)
    {
        return $this->getMapper()[$url] ?? $url;
    }

    /**
     * Put the mapper listing in a singleton variable if not done and return it.
     *
     * @return array The mapper listing.
     */
    public function getMapper()
    {
        if (!$this->mapper) {
            $this->loadMapper();
        }

        return $this->mapper;
    }

    /**
     * Put the mapper listing in a singleton variable.
     *
     * @return array The mapper listing.
     */
    private function loadMapper()
    {
        $arr = array();
        if (file_exists($this->mapperPath)) {
            include($this->mapperPath);
        }

        $this->mapper = $arr;
    }

    /**
     * Stub for the interface
     *
     * @param string $path A path
     */
    public function getVersion($path){}

    /**
     * Add an url in the asset mapper file
     *
     * @param string $url    The url to add.
     * @param string $busted The corresponding bust.
     *
     * @return array The asset mapper listing.
     */
    public function addUrl($url, $busted)
    {
        $mapper = $this->getMapper();
        $mapper[$url] = $busted;
        $this->setMapper($mapper);

        return $mapper;
    }

    /**
     * Save the mapper to a file and reset the singleton to null.
     *
     * @param array $arr The mapper to save
     */
    public function setMapper($arr)
    {
        $dir = dirname($this->mapperPath);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        file_put_contents($this->mapperPath, '<?php $arr = ' . var_export($arr, 1) . ';');
        $this->mapper = null;
    }

    /**
     * Remove the asset mapper.
     */
    public function removeMapper()
    {
        if (file_exists($this->mapperPath)) {
            unlink($this->mapperPath);
        }
        $this->mapper = null;
    }

    /**
     * Return the hash for the given asset.
     *
     * @param string $asset  The asset content or the asset path.
     *
     * @return bool|string
     */
    public function getHash($asset)
    {
        if ($asset[0] === '/') {
            if (!file_exists($asset)) {
                trigger_error('file not found: ' . $asset);

                return false;
            }
            $content = file_get_contents($asset);
        } else {
            $content = $asset;
        }
        $hash = hash_init('sha1');
        hash_update($hash, $content);

        return substr(hash_final($hash), 0, 10);
    }

    /**
     * Generate the final busted url from target path and web directory.
     *
     * @param string $target The final target not busted.
     * @param string $web    The web directory path.
     *
     * @return mixed
     */
    public function generateUrlFromWeb($target, $web)
    {
        $path = $web . $target;

        return $this->generateUrlFromPath($path, $target);
    }

    /**
     * Generate the final busted url from path with web url.
     *
     * @param string $path The file path.
     * @param string $url  The url to bust.
     *
     * @return mixed
     */
    public function generateUrlFromPath($path, $url)
    {
        if (!pathinfo($path, PATHINFO_EXTENSION)) {
            // nothing to replace because no extension
            return;
        }
        $hash = $this->getHash($path);
        if ($hash) {
            //@todo enough or should we build a stronger regexp ?
            if (strpos($url, $this->separator)) {
                return $url;
            }

            return $this->getHashedUrl($url, $hash);
        }
    }

    /**
     * Return the hashed url (always call here so if logic change, only here will be impacted).
     *
     * @param string $url  The normal url of the asset.
     * @param string $hash The hash of the asset.
     *
     * @return string The hashed url to call from client.
     */
    public function getHashedUrl($url, $hash)
    {
        $pos = strrpos($url, '.');
        return substr($url, 0, $pos) . $this->separator . $hash . substr($url, $pos);
    }
}
