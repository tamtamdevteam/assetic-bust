<?php

namespace TamTam\Assets\BustingBundle\Listener;

use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CommandListener
{
    /** @var EventDispatcherInterface The event dispatcher service */
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Find and execute the given command.
     *
     * @param ConsoleTerminateEvent $event     The base event.
     * @param string                $cmd       The command name.
     * @param array                 $arguments The arguments of the command.
     *
     * @throws \Exception
     */
    protected function execCommand(ConsoleTerminateEvent $event, $cmd, $arguments = [])
    {
        $event->getOutput()->writeln('AUTORUN: ' . $cmd);
        $command = $event->getCommand()->getApplication()->find($cmd);
        $arguments = array('command' => $cmd) + $arguments;
        $returnCode = $command->run(new ArrayInput($arguments), $event->getOutput());
        if ($returnCode) {
            throw new \Exception('Wrong return code for ' . $cmd . ' command: ' . $returnCode);
        }
        $ev = new ConsoleTerminateEvent($command, $event->getInput(), $event->getOutput(), $returnCode);

        $this->dispatcher->dispatch('console.terminate', $ev);
    }
}
