<?php

namespace TamTam\Assets\BustingBundle\Listener;

use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ClientBustingListener extends CommandListener
{
    /**
     * Automatically trigger the bust client after an bust assets.
     *
     * @param ConsoleTerminateEvent $event The sf console terminate event.
     */
    public function onConsoleTerminate(ConsoleTerminateEvent $event) {
        $command = $event->getCommand();
        if ($command->getName() === 'bust:assets') {
            $this->execCommand($event, 'bust:client');
        }
    }
}
