<?php

namespace TamTam\Assets\BustingBundle\Listener;

use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

class BustAssetsListener extends CommandListener
{
    /**
     * Automatically trigger the bust assets after an assetic dump.
     *
     * @param ConsoleTerminateEvent $event The sf console terminate event.
     */
    public function onConsoleTerminate(ConsoleTerminateEvent $event) {
        $command = $event->getCommand();

        if ($command->getName() === 'assetic:dump') {
            $this->execCommand($event, 'bust:assets');
        }
    }
}
