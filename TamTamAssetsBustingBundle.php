<?php

namespace TamTam\Assets\BustingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use TamTam\Assets\BustingBundle\DependencyInjection\TamTamAssetsBustingExtension;

class TamTamAssetsBustingBundle extends Bundle
{
    /**
     * This allow to change the alias of the bundle extension.
     * 
     * @return TamTamAssetsBustingExtension
     */
    public function getContainerExtension()
    {
        return new TamTamAssetsBustingExtension();
    }
}
