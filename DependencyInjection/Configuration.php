<?php

namespace TamTam\Assets\BustingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tamtam_assets_busting');
        $rootNode
            ->children()
                ->arrayNode('web')
                ->info('In this parameter you can put a list of path under web to bust the files in it.')
                    ->prototype('array')
                        ->children()
                        ->arrayNode('folders')
                            ->prototype('scalar')
                                ->info('The list of folder')
                                ->isRequired()
                            ->end()
                        ->end()
                        ->scalarNode('recurse')
                            ->info('Does the bust apply to subfolders too?')
                            ->defaultFalse()
                       ->end()
                       ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}

