# README #

# Bust managing for assetic in a SF3 application

#### Automatically generate and use a bust based on the content of the assets

This bundle provide a bust for each asset, automatically.

It also provide a bust filter for twig for dynamic asset (images).

To use, add the package to your composer.json

Enable it on the AppKernel.php file: 

> new TamTam\Assets\BustingBundle\TamTamAssetsBustingBundle()

Add the service to your assets configuration.

In config.yml (most of the time):

    framework:
        assets:
            version_strategy: tamtam_assets_busting.handler

The generated file is stored under: "%kernel.root_dir%/config/assetMapper.php"

To change this use the parameter: tamtam_assets_busting.mapper_path
