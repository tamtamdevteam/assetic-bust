<?php

namespace TamTam\Assets\BustingBundle\Twig;

use TamTam\Assets\BustingBundle\Handler\BustingStrategy;

/**
 * Custom twig extension to bust url
 */
class BustExtension extends \Twig_Extension
{
    /** @var BustingStrategy The mapper service */
    private $mapperService;

    public function __construct($mapperService)
    {
        $this->mapperService = $mapperService;
    }

    /**
     * Determines the functions available in the helper
     *
     * @return array The liszt of available functions
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('bust', array($this, 'bust')),
        );
    }

    /**
     * Return the given image url busted if possible.
     *
     * @param string $url The image path to bust.
     *
     * @return string The url busted if possible.
     */
    public function bust($url)
    {
        return $this->mapperService->applyVersion($url);
    }
}
