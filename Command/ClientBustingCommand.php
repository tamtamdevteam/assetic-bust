<?php

namespace TamTam\Assets\BustingBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClientBustingCommand extends \TamTam\StandardUIBundle\Command\AbstractCommand
{
    /**
     * Command configuration.
     */
    protected function configure()
    {
        $this
            ->setName('bust:client')
            ->setDescription('Generate a javascript which contains the busting mapper to bust images directly in js')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $this->getParameter('tamtam_assets_busting.asset_mapper_url');
        $buster = $this->get('tamtam_assets_busting.handler');
        $hash = $buster->getHash($this->getParameter('tamtam_assets_busting.mapper_path'));
        $mapper = $buster->addUrl($url, $buster->getHashedUrl($url, $hash));
        $dir = $this->get('kernel')->getWebDir();
        $content = '(function(){$(function(){t2.set("assets_mapper",' . json_encode($mapper, JSON_UNESCAPED_SLASHES) . ')})})();';

        if (file_put_contents($dir . $url, $content)) {
            $this->info('Bust mapper correctly extracted to ' . $url);
        } else {
            $this->error('An error occurred writing the file ' . $url);
        }
    }
}
