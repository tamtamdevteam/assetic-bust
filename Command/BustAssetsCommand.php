<?php

namespace TamTam\Assets\BustingBundle\Command;

use Assetic\Asset\AssetInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use TamTam\Assets\BustingBundle\Handler\BustingStrategy;

class BustAssetsCommand extends ContainerAwareCommand
{
    /** @var BustingStrategy the asset mapper service */
    private $mapperService;
    /** @var array the asset mapper */
    private $mapper;
    /** @var string the web folder path */
    private $web;

    /**
     * Command configuration.
     */
    protected function configure()
    {
        $this
            ->setName('bust:assets')
            ->setDescription('Generate the asset mapper based on the final assets');
    }

    /**
     * Generate the asset mapper file
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $am = $this->getContainer()->get('assetic.asset_manager');
        $this->mapperService = $this->getContainer()->get('tamtam_assets_busting.handler');
        $this->mapperService->checkDir();
        $this->mapper = $this->mapperService->getMapper();
        $this->web = $this->getContainer()->get('kernel')->getWebDir();
        foreach ($am->getNames() as $name) {
            $asset = $am->get($name);
            $this->bustAsset($asset, $output);
        }
        //@todo impossible to get directly the conf here ?
        if ($this->getContainer()->hasParameter('tamtam_assets_busting.config')) {
            $config = $this->getContainer()->getParameter('tamtam_assets_busting.config');
            foreach ($config['web'] as $info) {
                $recurse = $info['recurse'] ?? false;
                $output->writeln(
                    'Busting ' . ($recurse ? 'recursively ' : '') .
                    'web folders: ' . implode(', ', $info['folders'])
                );
                foreach ($this->getFiles($info['folders'], $recurse) as $file) {
                    $this->bust($file, $output);
                }
            }
        }
        $this->mapperService->setMapper($this->mapper);
    }

    /**
     * bust the given asset
     *
     * @param AssetInterface  $asset  The asset
     * @param OutputInterface $output The command output
     */
    private function bustAsset(AssetInterface $asset, OutputInterface $output) {
        $this->bust($this->web . $asset->getTargetPath(), $output);
    }

    /**
     * bust the given path
     *
     * @param string          $path   The asset path
     * @param OutputInterface $output The command output
     */
    private function bust($path, OutputInterface $output) {
        if (file_exists($path)) {
            $url = str_replace($this->web, '', $path);
            $busted = $this->mapperService->generateUrlFromPath($path, $url);
            $this->mapper[$url] = $busted;

            $output->writeln('File: ' . $url . ' busted name = ' . substr($busted, strrpos($busted, '/') + 1));
        } else {
            $output->writeln('FILE NOT FOUND : ' . $path);
        }
    }

    /**
     * Get all files in the folder, recursively if asked.
     *
     * @param array $folders The list of folders to parse.
     * @param bool  $recurse True if the listing should be recursive.
     *
     * @return array The list of unique files found.
     */
    public function getFiles(array $folders, bool $recurse)
    {
        $files = [];
        foreach ($folders as $path) {
            $path = $this->web . $path;
            if (!is_dir($path)) {
                continue;
            }
            if ($recurse) {
                $list = new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator($path),
                    \RecursiveIteratorIterator::LEAVES_ONLY
                );
            } else {
                $list = new \FilesystemIterator($path);
            }
            $files += array_keys(array_filter(iterator_to_array($list), function($file) {
                return $file->isFile();
            }));
        }

        return array_unique($files);
    }
}